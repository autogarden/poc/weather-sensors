# Installing requirements

```bash
sudo apt-get install python3-pip
sudo apt-get install libgpiod2
sudo pip3 install --upgrade setuptools
sudo pip3 install Adafruit-Blinka
sudo pip3 install adafruit-circuitpython-dht


sudo pip3 install RPI.GPIO
sudo pip3 install adafruit-blinka
```

## Configuring I2C
 
* https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

```bash
sudo apt-get install -y python-smbus
sudo apt-get install -y i2c-tools
sudo raspi-config
```
* 3 Interface options
* P5 I2C
* Enable

```bash
sudo reboot
```

Testing i2c

```bash
sudo i2cdetect -y 1
```

## Configuring SPI

```bash
sudo raspi-config
```
* 3 Interface options
* P4 SPI
* Enable

```bash
sudo reboot
```

Testing SPI

```bash
ls -l /dev/spidev*
```

 
## Testing setup

```bash
python3 examples/blinkatest.py
``` 