# Observability components


## Prometheus & Grafana

* http://www.d3noob.org/2020/02/installing-prometheus-and-grafana-on.html

### Upgrade Raspberry Pi

```bash
sudo apt-get update
sudo apt-get upgrade
```

### Installing Prometheus

```bash
export PROMETHEUS_VERSION=2.23.0
export PROMETHEUS_FILE=prometheus-${PROMETHEUS_VERSION}.linux-armv7.tar.gz

cd /opt
wget https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/$PROMETHEUS_FILE
tar xzf $PROMETHEUS_FILE
rm $PROMETHEUS_FILE
mv prometheus-${PROMETHEUS_VERSION}.linux-armv7/ prometheus/
   
cat > /etc/systemd/system/prometheus.service <<EOF
[Unit]
Description=Prometheus Server
Documentation=https://prometheus.io/docs/introduction/overview/
After=network-online.target
[Service]
User=root
Restart=on-failure
ExecStart=/opt/prometheus/prometheus --config.file=/opt/prometheus/prometheus.yml --storage.tsdb.path=/opt/prometheus/data
[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl start prometheus 
sudo systemctl status prometheus 
sudo systemctl enable prometheus
```

http://192.168.1.108:9090/

### Installing Grafana

```bash
export GRAFANA_VERSION=7.3.4
export GRAFANA_FILE=grafana-${GRAFANA_VERSION}.linux-armv7.tar.gz
wget https://dl.grafana.com/oss/release/${GRAFANA_FILE}
tar xfz ${GRAFANA_FILE}   
rm ${GRAFANA_FILE}   
mv grafana-${GRAFANA_VERSION}/ grafana/

cat > /etc/systemd/system/grafana.service <<EOF   
[Unit]
Description=Grafana Server
After=network.target
[Service]
Type=simple
User=root
ExecStart=/opt/grafana/bin/grafana-server
WorkingDirectory=/opt/grafana/
Restart=always
RestartSec=10
[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl start grafana
sudo systemctl status grafana
sudo systemctl enable grafana
```

http://192.168.1.108:3000

Username: `admin/admin`

#### Add datasource

Add prometheus datasource in Grafana
URL: `http://localhost:9090`
Method: `GET`


## Intall MQTT.FX

```bash
zypper install http://www.jensd.de/apps/mqttfx/1.7.1/mqttfx-1.7.1-1.x86_64.rpm
```

## Install node exporter

```bash
sudo apt-get update
sudo apt-get install prometheus-node-exporter
sudo systemctl enable prometheus-node-exporter
sudo systemctl start prometheus-node-exporter
```

## Configure prometheus for node exporter

```bash
cat >> /opt/prometheus/prometheus.yml <<EOF
  - job_name: 'openwebrx'
    scrape_interval: 5s
    static_configs:
    - targets: ['localhost:9100'] 
EOF
```
```bash
systemctl restart prometheus
```

## Add dashboard in Grafana

* https://grafana.com/grafana/dashboards/1860
