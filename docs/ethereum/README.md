

```
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install git libgmp3-dev
```

```bash
mkdir -p ~/src && cd ~/src
wget https://dl.google.com/go/go1.15.6.linux-armv6l.tar.gz
sudo tar -C /usr/local -xzf go1.15.6.linux-armv6l.tar.gz
rm go1.15.6.linux-arm6l.tar.gz

sudo cat >> ~/.bashrc <<EOF
PATH=\$PATH:/usr/local/go/bin
GOPATH=$HOME/go
EOF
```

```bash
cd ~
mkdir  -p src
cd src
git clone https://github.com/ethereum/go-ethereum
cd go-ethereum
make
sudo cp build/bin/geth /usr/local/bin/
```

```bash
geth account new
geth --syncmode light --cache 64 --maxpeers 12
```

```bash
sudo cat > /etc/systemd/system/geth@.service <<EOF
[Unit]
Description=Ethereum daemon
Requires=network.target
[Service]
Type=simple
User=%I
ExecStart=/usr/local/bin/geth --syncmode light --cache 64 --maxpeers 12
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable geth@pi.service
sudo systemctl start geth@pi.service
```

```bash
geth attach
```

```bash
> eth.accounts
> admin.peers
```

```bash
sudo systemctl stop geth@pi.service
sudo systemctl disable geth@pi.service
```

```bash
geth --datadir .s3-audit account new

cat > s3-audit.json <<EOF
{
    "config": {
        "chainId": 555,
        "homesteadBlock": 0
    },
    "difficulty": "20",
    "gasLimit": "2100000",
    "alloc": {
        "0x1F28c226840F1bBcB879Ff9dd339041d002AC2bf":
        { "balance": "20000000000000000000" }
    }
}
EOF

geth --datadir .s3-audit init s3-audit.json

geth --identity chainpi --rpc --rpcport 8080 --rpccorsdomain "*" --datadir .s3-audit --port 30303 --nodiscover --rpcapi "db,eth,net,web3" --networkid 555 console
```

```bash
docker run -d --name ethereum-node -v /Users/alice/ethereum:/root -p 8545:8545 -p 30303:30303 ethereum/client-go
```