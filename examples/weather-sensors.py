#!/usr/bin/env python

# RaspberryPi modules
import RPi.GPIO as GPIO
from picamera import PiCamera,color
import picamera 

# some strange module
import ConfigParser
import sys,json,getopt,datetime,time,subprocess,uuid,os,io,syslog
from time import sleep
import base64
from codenamize import codenamize
import concurrent.futures
import requests
from requests_futures.sessions import FuturesSession
from requests_futures.sessions import FuturesSession


__author__ = "Fernando Hakcbart"
__version__ = "1.0"
__maintainer__ = "Fernando Hackbart"

endpoint=''
device_id='-'
device_name=codenamize("weather-sensors-"+uuid.uuid4().urn)
config_folder="/etc/weather-sensors"
config_file="/etc/weather-sensors/weather-sensors.conf"
service_file="/lib/systemd/system/weather-sensors.service"

pir_sensor = 12
speaker= 16
ultrasonic_trigger = 18
ultrasonic_echo = 24
request_timeout = 10
#retention_seconds = 2 * 86400 #two days
retention_seconds = 14400 #four hours

add_service = False
remove_service = False
configure_camera = False
staging_folder = "/var/tmp"

def getconfig():
global endpoint
global device_id  
global staging_folder
global configure_camera 
global device_name
configDisk = ConfigParser.RawConfigParser()   
configDisk.read(config_file)
device_id = configDisk.get('weather-sensors', 'device_id')
endpoint = configDisk.get('weather-sensors', 'endpoint')
configure_camera = configDisk.get('weather-sensors', 'configure_camera')
staging_folder = configDisk.get('weather-sensors', 'staging_folder')
device_name = configDisk.get('weather-sensors', 'device_name')
syslog.syslog(syslog.LOG_INFO,"Config for "+device_name+" exists in "+endpoint)

def saveconfig():
config = ConfigParser.RawConfigParser()
config.add_section('weather-sensors')
config.set('weather-sensors', 'endpoint', endpoint)
config.set('weather-sensors', 'device_id', device_id)
config.set('weather-sensors', 'configure_camera', configure_camera)
config.set('weather-sensors', 'staging_folder', staging_folder)
config.set('weather-sensors', 'device_name', device_name)
with open(config_file, 'wb') as cfn:
  config.write(cfn)
cfn.close()

def createdevice():
global device_id
global device_name
#generate a name
#device_name = subprocess.call(["cat","/proc/cpuinfo","|","grep","Serial","|","awk", "-F':'","'{ print $2 }'","|","tr" "-d \" \""])
syslog.syslog(syslog.LOG_INFO,"Checking of device "+device_name+" exists in "+endpoint)
datareq = '{"name": "%s"}' % (device_name)
try:
    responsecreate = requests.post(endpoint, data=datareq,headers={"Content-Type": "application/json"}, timeout=request_timeout)
    j = json.loads(responsecreate.content)
    syslog.syslog(syslog.LOG_INFO,"ID received on device creation: "+j['id'])
    device_id=j['id']
except requests.exceptions.Timeout:
    device_id=str(uuid.uuid4())
    syslog.syslog(syslog.LOG_ERR,"Timeout connecting to "+endpoint+" forcing device_id: "+device_id)
except requests.exceptions.TooManyRedirects:
    device_id=str(uuid.uuid4())
    syslog.syslog(syslog.LOG_WARNING,"TooManyRedirects on "+endpoint+" forcing device_id: "+device_id)
except requests.exceptions.ConnectionError as e:
    device_id=str(uuid.uuid4())
    syslog.syslog(syslog.LOG_ERR,"ConnectionError on "+endpoint+" forcing device_id: "+device_id +" error: "+str(e))
except requests.exceptions.RequestException as e:
    device_id=str(uuid.uuid4())
    syslog.syslog(syslog.LOG_ERR,"Erro connecting to "+endpoint+" forcing device_id: "+device_id +" error: "+str(e))

def configure():
if not os.path.exists(config_folder):
  syslog.syslog(syslog.LOG_INFO,"Creating configuration folder: "+config_folder)
  os.makedirs(config_folder)

if not os.path.exists(config_file):
  syslog.syslog(syslog.LOG_INFO,"Creating configuration file: "+config_file)
  saveconfig()
else:
  getconfig()

if device_id == "-":
  createdevice()

syslog.syslog(syslog.LOG_INFO,"Checking if device "+device_id+" exists in "+endpoint)

get_status_code=404

try:
  responseget = requests.get(endpoint+"/"+device_id,headers={"Content-Type": "application/json"}, timeout=request_timeout)
  get_status_code=responseget.status_code

except requests.exceptions.Timeout:
    syslog.syslog(syslog.LOG_ERR,"Timeout connecting to "+endpoint)
    get_status_code=404
except requests.exceptions.TooManyRedirects:
    syslog.syslog(syslog.LOG_WARNING,"TooManyRedirects on "+endpoint)
    get_status_code=404  
except requests.exceptions.ConnectionError as e:
    syslog.syslog(syslog.LOG_ERR,"ConnectionError on "+endpoint+" error: "+str(e))
    get_status_code=404
except requests.exceptions.RequestException as e:
    syslog.syslog(syslog.LOG_ERR,"Erro connecting to "+endpoint+" error: "+str(e))
    get_status_code=404

if get_status_code==404:
  createdevice()

saveconfig()

def addservice():
syslog.syslog(syslog.LOG_INFO,"Creating service file: "+service_file)
with open(service_file, 'w') as fo:
  fo.write("[Unit]\nDescription=Weather sensors (IoT edge device)\nAfter=multi-user.target\nConflicts=getty@tty1.service\n\n[Service]\nType=simple\nExecStart=/usr/bin/python /usr/bin/weather-sensors.py\nStandardInput=tty-force\n\n[Install]\nWantedBy=multi-user.target")
fo.close()
syslog.syslog(syslog.LOG_INFO,"Reloading systemctl daemon")
subprocess.call(["systemctl", "daemon-reload"])
syslog.syslog(syslog.LOG_INFO,"Enabling weather-sensors.service ")
subprocess.call(["systemctl", "enable","weather-sensors.service"])
syslog.syslog(syslog.LOG_INFO,"Starting weather-sensors.service ")
subprocess.call(["systemctl", "start","weather-sensors.service"])

def removeservice():
syslog.syslog(syslog.LOG_INFO,"Stopping weather-sensors.service ")
subprocess.call(["systemctl", "stop","weather-sensors.service"])
syslog.syslog(syslog.LOG_INFO,"Disabling weather-sensors.service ")
subprocess.call(["systemctl", "disable","weather-sensors.service"])
syslog.syslog(syslog.LOG_INFO,"Removing service file: "+service_file)
subprocess.call(["rm", service_file])
syslog.syslog(syslog.LOG_INFO,"Removing config file: "+config_file)
subprocess.call(["rm", config_file])
syslog.syslog(syslog.LOG_INFO,"Reloading systemctl daemon")
subprocess.call(["systemctl", "daemon-reload"])
subprocess.call(["systemctl", "reset-failed"])

def cleanup_stage():
now = time.time()
for f in os.listdir(staging_folder):
  if os.stat(os.path.join(staging_folder,f)).st_mtime < now - retention_seconds:
    if os.path.isfile(f):
      syslog.syslog(syslog.LOG_DEBUG,"Removing: "+os.path.join(staging_folder, f))
      os.remove(os.path.join(staging_folder, f))

def measure():
current_state = 0
GPIO.setmode(GPIO.BCM)
GPIO.setup(pir_sensor, GPIO.IN)
GPIO.setup(speaker, GPIO.OUT)
GPIO.setup(ultrasonic_trigger, GPIO.OUT)
GPIO.setup(ultrasonic_echo, GPIO.IN)  
try:
  #session = FuturesSession(max_workers=10)
  
  if (configure_camera == "True"):
    syslog.syslog(syslog.LOG_INFO,"Configuring camera 2592 x 1944")
    camera = PiCamera()  
    camera.resolution = (2592,1944)
    camera.annotate_background = color.Color('black')
    camera.annotate_foreground = color.Color('green')
    camera.annotate_text_size = 50  
    camera.led = False  
    #camera.CAPTURE_TIMEOUT = 5  
    #https://picamera.readthedocs.io/en/release-1.13/api_camera.html?highlight=camera%20annotate_text_size#picamera.PiCamera.annotate_background

  while True:
    time.sleep(0.1)
    current_state = GPIO.input(pir_sensor)
    if current_state == 1:
      syslog.syslog(syslog.LOG_INFO,"Movement detected!")
      # make some noise 
      GPIO.output(speaker,True)
      time.sleep(0.25)
      GPIO.output(speaker,False)
      # take ten measures to try to identify something
      for seq in range(10):
        dist = distance()

        # send GPU temp /opt/vc/bin/vcgencmd measure_temp
        # send CPU temp cpu=$(</sys/class/thermal/thermal_zone0/temp) && $((cpu/1000))

        read_id = str(uuid.uuid4())
        read_date = datetime.datetime.now()
        read_date_string = read_date.strftime("%Y%m%H%M%S%f")
        read_file_prefix = staging_folder+"/weather-sensors-"+read_date_string+"-"+read_id
        if (configure_camera == "True"):
            image_file = read_file_prefix+".jpg"
            try:
              camera.start_preview()
              time.sleep(0.25)
              camera.annotate_text = "distance: %i, time: %s" % (dist,read_date_string)
              camera.capture(image_file)
              with open(image_file,"rb") as ii:
                data = '{"device": "%s", "read": "%s", "sequence": %i, "instant": "%s", "distance": %i, "imageb64": "%s"}' % (device_id,uuid.uuid4(),seq,read_date,dist,base64.b64encode(ii.read()))
              ii.close()
              os.remove(image_file)
            except IOError as e:
              syslog.syslog(syslog.LOG_ERR,"Error generating image: "+read_file_prefix+".jpg "+str(e))
              data = '{"device": "%s", "read": "%s", "sequence": %i, "instant": "%s", "distance": %i}' % (device_id,read_id,seq,read_date,dist)
            except (picamera.PiCameraError, picamera.PiCameraValueError, picamera.PiCameraRuntimeError ) as e:
              syslog.syslog(syslog.LOG_ERR,"Error generating image: "+read_file_prefix+".jpg "+str(e))
              data = '{"device": "%s", "read": "%s", "sequence": %i, "instant": "%s", "distance": %i}' % (device_id,read_id,seq,read_date,dist)
        else:
            data = '{"device": "%s", "read": "%s", "sequence": %i, "instant": "%s", "distance": %i}' % (device_id,read_id,seq,read_date,dist)
        
        # Writing down payload file (how to handle out of disk errors?)
        try:
          payload_file = read_file_prefix+".json" 
          with open(payload_file, 'wb') as da:
            da.write(data)
          da.close()
        except IOError as e:
          syslog.syslog(syslog.LOG_ERR,"Error writing payload file at "+read_file_prefix+".json "+str(e))


        # sending the request to backend          
        try:
            #session.post(endpoint+"/"+device_id, data=data,headers={"Content-Type": "application/json"}, timeout=request_timeout)
            requests.post(endpoint+"/"+device_id, data=data,headers={"Content-Type": "application/json"}, timeout=request_timeout)
        except requests.exceptions.Timeout:
            syslog.syslog(syslog.LOG_ERR,"Timeout connecting to "+endpoint)
        except requests.exceptions.TooManyRedirects:
            syslog.syslog(syslog.LOG_WARNING,"TooManyRedirects on "+endpoint)
        except requests.exceptions.ConnectionError as e:
            syslog.syslog(syslog.LOG_ERR,"ConnectionError on "+endpoint+" error: "+str(e))
        except requests.exceptions.RequestException as e:
            syslog.syslog(syslog.LOG_ERR,"Erro connecting to "+endpoint+" error: "+str(e))    

        time.sleep(0.5)
        cleanup_stage()
except KeyboardInterrupt:
  pass
finally:
  GPIO.cleanup()       

def distance():
GPIO.output(ultrasonic_trigger, True)
time.sleep(0.00001)
GPIO.output(ultrasonic_trigger, False)
StartTime = time.time()
StopTime = time.time()
while GPIO.input(ultrasonic_echo) == 0:
  StartTime = time.time()
while GPIO.input(ultrasonic_echo) == 1:
  StopTime = time.time()
TimeElapsed = StopTime - StartTime
distance = (TimeElapsed * 34300) / 2
return distance

def main(argv):
global endpoint
global add_service
global remove_service
global configure_camera
global staging_folder

help_string = 'weather-sensors.py -u|--ioturl=<weather_sensors_endpoint> [-f|--stagingfolder=<staging_folder>] [-a|--addservice] [-r|--removeservice] [-c|--configurecamera]'

try:
  opts, args = getopt.getopt(argv,"harcf:u:",["addservice","removeservice","configurecamera","stagingfolder=","ioturl="])
except getopt.GetoptError:
  print(help_string)
  sys.exit(2)
for opt, arg in opts:
  if opt == '-h':
    print(help_string)
    sys.exit()
  elif opt in ("-u", "--ioturl"):
    endpoint = arg
  elif opt in ("-a", "--addservice"):
    add_service = True
  elif opt in ("-c", "--configurecamera"):
    configure_camera = "True"      
  elif opt in ("-f", "--stagingfolder"):
    staging_folder = arg
  elif opt in ("-r", "--removeservice"):
    remove_service = True

if add_service:
  syslog.syslog(syslog.LOG_INFO,"Configuring weather-sensors: "+device_name+", with ["+endpoint+"] as endpoint and staging folder: "+staging_folder)
  if (configure_camera == "True"):
    syslog.syslog(syslog.LOG_WARNING,"Configuring weather-sensors with camera")

  syslog.syslog(syslog.LOG_WARNING,"This file should be saved as /usr/bin/weather-sensors.py")  

  configure()
  addservice()

if remove_service:
  syslog.syslog(syslog.LOG_WARNING,"Removing weather-sensors")
  removeservice()


if ((not add_service) and (not remove_service)):
  getconfig()        
  syslog.syslog(syslog.LOG_INFO,"Measuring with weather-sensors "+device_name+" with endpoint: "+endpoint+" and device_id: "+device_id)
  measure()

if __name__ == '__main__':
main(sys.argv[1:])    
