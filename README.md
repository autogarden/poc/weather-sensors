# weather-sensors

Raspberry Pi weather sensors in Python

## Motivation

Create a growing station to follow the conditions of plants.

## Ideas sources

* https://tutorials-raspberrypi.com/raspberry-pi-sensors-overview-50-important-components/
* https://tutorials-raspberrypi.com/raspberry-pi-security-camera-livestream-setup/

* https://pinout.xyz/#

Updates:

* https://circuitpython.org/blinka
* https://github.com/adafruit/Adafruit_Blinka

Adafruit:

* https://circuitpython.org/blinka
* https://circuitpython.org/blinka/raspberry_pi_3bplus/
* https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi

[Setup](docs/setup/setup.md)

## Running examples

* [DHT22](examples/dht22/DHT22.md)
* [BMP180](examples/bmp108/BMP180.md)
* [mcp3008](examples/mcp3008/mcp3008.md)
* [bright](examples/bright/bright.md)
* [soil](examples/soil/soil.md)

## Exposing the metrics collected to Prometheus scrapers

